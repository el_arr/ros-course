#ifndef FOLLOWER_H
#define FOLLOWER_H

#include <ros/ros.h>
#include <image_transport/image_transport.h>

class Follower {
private:
    ros::NodeHandle nh;
    image_transport::ImageTransport imageTransport;
    image_transport::Subscriber imageSubscriber;

    void imageCallback(const sensor_msgs::ImageConstPtr& msg);
    ros::Publisher cmdVelPublisher;

public:
    Follower();
    virtual ~Follower();
};

#endif // FOLLOWER_H
