#include <boost/shared_ptr.hpp>
#include <pluginlib/class_loader.h>
#include "calculator_base.h"

int main(int argc, char **argv) {
    pluginlib::ClassLoader <calculator_base::calc_functions> calc_loader("calculator_pluginlib",
                                                                         "calculator_base::calc_functions");

    try {
        boost::shared_ptr <calculator_base::calc_functions> add = calc_loader.createInstance(
                "calculator_pluginlib/Add");
        add->get_numbers(10.0, 10.0);
        double result = add->operation();
        ROS_INFO("Result of add 10.0 and 10.0: %.2f", result);
    }
    catch (pluginlib::PluginlibException &ex) {
        ROS_ERROR("The plugin failed to load for some reason. Error: %s", ex.what());
    }

    try {
        boost::shared_ptr <calculator_base::calc_functions> sub = calc_loader.createInstance(
                "calculator_pluginlib/Sub");
        sub->get_numbers(10.0, 10.0);
        double result = sub->operation();
        ROS_INFO("Result of sub 10.0 and 10.0: %.2f", result);
    }
    catch (pluginlib::PluginlibException &ex) {
        ROS_ERROR("The plugin failed to load for some reason. Error: %s", ex.what());
    }

    try {
        boost::shared_ptr <calculator_base::calc_functions> mul = calc_loader.createInstance(
                "calculator_pluginlib/Mul");
        mul->get_numbers(10.0, 10.0);
        double result = mul->operation();
        ROS_INFO("Result of mul 10.0 and 10.0: %.2f", result);
    }
    catch (pluginlib::PluginlibException &ex) {
        ROS_ERROR("The plugin failed to load for some reason. Error: %s", ex.what());
    }

    try {
        boost::shared_ptr <calculator_base::calc_functions> div = calc_loader.createInstance(
                "calculator_pluginlib/Div");
        div->get_numbers(10.0, 10.0);
        double result = div->operation();
        ROS_INFO("Result of div 10.0 and 10.0: %.2f", result);
    }
    catch (pluginlib::PluginlibException &ex) {
        ROS_ERROR("The plugin failed to load for some reason. Error: %s", ex.what());
    }

    return 0;
}
