#ifndef TURNLEFT_H
#define TURNLEFT_H

#include "Behavior.h"
#include <ros/ros.h>
#include "sensor_msgs/LaserScan.h"
#include <cmath>

class TurnLeft : public Behavior {
public:
    TurnLeft();

    virtual bool startCond();

    virtual void action();

    virtual bool stopCond();

    virtual ~TurnLeft();

private:
    constexpr static double TURN_SPEED_MPS = 1.0;
    constexpr static double MIN_SCAN_ANGLE = M_PI + (-30.0 / 180 * M_PI);
    constexpr static double MAX_SCAN_ANGLE = M_PI + (0.0 / 180 * M_PI);
    constexpr static float MIN_PROXIMITY_RANGE_M = 0.6;

    ros::NodeHandle node;
    ros::Publisher commandPub;
    ros::Subscriber laserSub;

    void scanCallback(const sensor_msgs::LaserScan::ConstPtr &scan);

    bool keepTurningLeft;
};

#endif // TURNLEFT_H
