#ifndef PLAN_H
#define PLAN_H

#include "Behavior.h"

class Plan {
public:
    Plan();

    Behavior *getStartBehavior();

    virtual ~Plan();

protected:
    vector<Behavior *> behaviors;
    Behavior *startBehavior;
};

#endif // PLAN_H
