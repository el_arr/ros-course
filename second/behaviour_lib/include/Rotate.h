#ifndef ROTATE_H
#define ROTATE_H

#include "Behavior.h"
#include <ros/ros.h>
#include "sensor_msgs/LaserScan.h"
#include <cmath>

class Rotate : public Behavior {
public:
	Rotate();

	virtual bool startCond();

	virtual void action();

	virtual bool stopCond();

	virtual ~Rotate();

private:
	constexpr static double TURN_SPEED_MPS = 1.0;

	ros::NodeHandle node;
	ros::Publisher commandPub;

	bool keepTurning;

	static int random(int min, int max);
};

#endif // ROTATE_H
