#ifndef OBSTACLEAVOIDPLAN_H
#define OBSTACLEAVOIDPLAN_H

#include "Plan.h"

class ObstacleAvoidPlan : public Plan {
public:
    ObstacleAvoidPlan();

    virtual ~ObstacleAvoidPlan();
};

#endif // OBSTACLEAVOIDPLAN_H
