#include "TurnLeft.h"
#include "geometry_msgs/Twist.h"

TurnLeft::TurnLeft() {
    commandPub = node.advertise<geometry_msgs::Twist>("cmd_vel", 10);
    laserSub = node.subscribe("scan", 1, &TurnLeft::scanCallback, this);
    keepTurningLeft = true;
}

bool TurnLeft::startCond() {
    return keepTurningLeft;
}

void TurnLeft::action() {
    geometry_msgs::Twist msg;
    if (keepTurningLeft) {
        msg.angular.z = TURN_SPEED_MPS;
        ROS_INFO("Turning left");
    } else {
        msg.angular.z = 0;
        ROS_INFO("Stop turning left");
    }
    commandPub.publish(msg);
}

bool TurnLeft::stopCond() {
    return !keepTurningLeft;
}

void TurnLeft::scanCallback(const sensor_msgs::LaserScan::ConstPtr &scan) {
    // Find the closest range between the defined minimum and maximum angles
    int minIndex = ceil((MIN_SCAN_ANGLE - scan->angle_min) / scan->angle_increment);
    int maxIndex = floor((MAX_SCAN_ANGLE - scan->angle_min) / scan->angle_increment);

    float closestRange = scan->ranges[minIndex];
    for (int currIndex = minIndex + 1; currIndex <= maxIndex; currIndex++) {
        if (scan->ranges[currIndex] < closestRange) {
            closestRange = scan->ranges[currIndex];
        }
    }

    if (closestRange < MIN_PROXIMITY_RANGE_M) {
        keepTurningLeft = true;
    } else {
        keepTurningLeft = false;
    }
}

TurnLeft::~TurnLeft() {
}
