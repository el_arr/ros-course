#include <random>

#include "Rotate.h"
#include "geometry_msgs/Twist.h"

Rotate::Rotate() {
	commandPub = node.advertise<geometry_msgs::Twist>("cmd_vel", 10);
	keepTurning = true;
}

bool Rotate::startCond() {
	return keepTurning;
}

void Rotate::action() {
	geometry_msgs::Twist msg;
	int direction = random(0, 1);
	if (direction > 0) {
		direction = 1;
	} else {
		direction = -1;
	}
	if (keepTurning) {
		int duration = random (2, 4);
		msg.angular.z = direction * TURN_SPEED_MPS;
		ROS_INFO("Rotating");
		commandPub.publish(msg);
		ros::Duration(duration).sleep();
		this->keepTurning = false;
	} else {
		msg.angular.z = 0;
		ROS_INFO("Stop rotating");
		commandPub.publish(msg);
	}

}

bool Rotate::stopCond() {
	return !keepTurning;
}

Rotate::~Rotate() {}

int Rotate::random(int min, int max) {
	std::random_device rd;
	std::mt19937 rng(rd());
	std::uniform_int_distribution<int> uid(min, max);

	return uid(rng);
}