#include "MoveForward.h"
#include "geometry_msgs/Twist.h"

MoveForward::MoveForward() {
    commandPub = node.advertise<geometry_msgs::Twist>("cmd_vel", 10);
    laserSub = node.subscribe("scan", 1, &MoveForward::scanCallback, this);
    keepMovingForward = true;
}

bool MoveForward::startCond() {
    return keepMovingForward;
}

void MoveForward::action() {
    geometry_msgs::Twist msg;
    if (keepMovingForward) {
        msg.linear.x = (-1) * FORWARD_SPEED_MPS;
        ROS_INFO("Moving forward");
    } else {
        msg.linear.x = 0;
        ROS_INFO("Stop moving forward");
    }
    commandPub.publish(msg);
}

bool MoveForward::stopCond() {
    return !keepMovingForward;
}

void MoveForward::scanCallback(const sensor_msgs::LaserScan::ConstPtr &scan) {
    // Find the closest range between the defined minimum and maximum angles
    int minIndex = ceil((MIN_SCAN_ANGLE - scan->angle_min) / scan->angle_increment);
    int maxIndex = floor((MAX_SCAN_ANGLE - scan->angle_min) / scan->angle_increment);

    float closestRange = scan->ranges[minIndex];
    for (int currIndex = minIndex + 1; currIndex <= maxIndex; currIndex++) {
        if (scan->ranges[currIndex] < closestRange) {
            closestRange = scan->ranges[currIndex];
        }
    }

    if (closestRange < MIN_PROXIMITY_RANGE_M) {
        keepMovingForward = false;
    } else {
        keepMovingForward = true;
    }
}

MoveForward::~MoveForward() {
}
