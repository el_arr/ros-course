#include "Manager.h"
#include "ObstacleAvoidPlan.h"
#include <ros/ros.h>

int main(int argc, char **argv) {
    ros::init(argc, argv, "behavior_wanderer");

    ObstacleAvoidPlan plan;
    Manager manager(&plan);

    // Start the movement
    manager.run();

    return 0;
};
