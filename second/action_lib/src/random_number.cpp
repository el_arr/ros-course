#include <ros/ros.h>
#include <std_msgs/Float32.h>
#include <random>

int main(int argc, char **argv) {
    ros::init(argc, argv, "random_number");
    ros::NodeHandle nh;
    ros::Publisher pub = nh.advertise<std_msgs::Float32>("/random_number", 1);

    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_int_distribution<int> dist(1, 100);

    ros::Rate loop_rate(10);
    while (ros::ok()) {
        std_msgs::Float32 msg;
        msg.data = dist(mt);
        pub.publish(msg);
        ros::spinOnce();
        loop_rate.sleep();
    }
    return 0;
}
