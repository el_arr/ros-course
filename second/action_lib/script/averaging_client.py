#! /usr/bin/env python

from __future__ import print_function

import sys

import rospy
# Brings in the SimpleActionClient
import actionlib

# Brings in the messages used by the fibonacci action, including the
# goal message and the result message.
import action_lib.msg


def averaging_client():
    client = actionlib.SimpleActionClient('averaging', action_lib.msg.AveragingAction)
    client.wait_for_server()

    goal = action_lib.msg.AveragingGoal(samples=100)
    client.send_goal(goal)

    if client.wait_for_result(rospy.Duration.from_sec(30.0)):
        state = client.get_state()
        rospy.loginfo("Action finished: {}".format(state))
        rospy.loginfo("Mean: {}".format(client.get_result().mean))
        rospy.loginfo("Std: {}".format(client.get_result().std_dev))
    else:
        rospy.loginfo("Action did not finish before the time out.")


if __name__ == '__main__':
    try:
        rospy.init_node('test_averaging')
        averaging_client()
    except rospy.ROSInterruptException:
        print("program interrupted before completion", file=sys.stderr)
