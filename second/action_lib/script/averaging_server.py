#! /usr/bin/env python

import rospy

import actionlib

import action_lib.msg

from std_msgs.msg import Float32
from math import pow, sqrt, fabs


class AveragingAction(object):
    _feedback = action_lib.msg.AveragingActionFeedback()
    _result = action_lib.msg.AveragingActionResult()

    def __init__(self, name):
        self._action_name = name
        self._as = actionlib.SimpleActionServer(self._action_name, action_lib.msg.AveragingAction, auto_start=False)

        self._as.register_goal_callback(self.goalCB)
        self._as.register_preempt_callback(self.preemptCB)

        # subscribe to the data topic of interest
        self._sub = rospy.Subscriber("/random_number", Float32, callback=self.analysisCB)
        self._as.start()

    def goalCB(self):
        # reset helper variables
        self._data_count = 0
        self._sum = 0
        self._sum_sq = 0
        # accept the new goal
        self._goal = self._as.accept_new_goal().samples

    def preemptCB(self):
        rospy.loginfo("{} : Preempted".format(self._action_name))
        self._as.set_preempted()

    def analysisCB(self, msg):
        # make sure that the action hasn't been canceled
        if not self._as.is_active():
            return

        self._data_count += 1
        self._feedback.feedback.sample = self._data_count
        self._feedback.feedback.data = msg.data

        # compute the std_dev and mean of the data
        self._sum += msg.data
        self._feedback.feedback.mean = self._sum / self._data_count
        self._sum_sq += msg.data * msg.data
        self._feedback.feedback.std_dev = sqrt(
            fabs((self._sum_sq / self._data_count) - pow(self._feedback.feedback.mean, 2)))
        self._as.publish_feedback(self._feedback.feedback)

        if self._data_count > self._goal:
            self._result.result.mean = self._feedback.feedback.mean
            self._result.result.std_dev = self._feedback.feedback.std_dev

            if self._result.result.mean < 5.0:
                rospy.loginfo("{} : Aborted".format(self._action_name))
                self._as.set_aborted(self._result.result)
            else:
                rospy.loginfo("{} : Succeeded".format(self._action_name))
                self._as.set_succeeded(self._result.result)


if __name__ == '__main__':
    rospy.init_node('averaging')
    server = AveragingAction(rospy.get_name())
    rospy.spin()
