#include <ros/ros.h>
#include <ros/console.h>
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/LaserScan.h>
#include <sensor_msgs/image_encodings.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

using namespace cv;
using namespace std;

class Dumpster {
private:
	ros::NodeHandle nodeHandle;
	ros::Publisher cmdVelPublisher;
	ros::Subscriber laserSubscriber;
	image_transport::Subscriber cameraSubscriber;
	image_transport::ImageTransport imageTransport;

	const string IMAGE_WINDOW = "Image window";
	const Size IMAGE_SIZE = Size(720, 480);

	const int SAFE_DISTANCE = 1;
	const double SPEED_X = 0.2;
	const double SPEED_Z = 15;

	double err = 0;

	bool safe = false;
	bool found = false;
	bool diagonal = false;
	bool front = false;
	bool side = false;
	bool partial = false;
	bool left = false;
	bool right = false;
	bool done = false;

	void cameraImageCallback(const sensor_msgs::ImageConstPtr &msg) {
		cv_bridge::CvImagePtr cv_ptr;
		try {
			cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
		} catch (cv_bridge::Exception &e) {
			ROS_ERROR("cv_bridge exception: %s", e.what());
			return;
		}

		Mat image = cv_ptr->image;
		resize(image, image, IMAGE_SIZE);

		Mat result, mask;
		cvtColor(image, result, CV_BGR2HSV);
		Scalar lower(36, 0, 0);
		Scalar upper(86, 255, 255);
		inRange(result, lower, upper, mask);

		vector <vector<Point>> contours;
		vector <Vec4i> hierarchy;
		findContours(mask, contours, hierarchy, RETR_LIST, CHAIN_APPROX_SIMPLE);

		result = image.clone();
		if (contours.size() != 0) {
			int max_contour_size = contours[0].size();
			int max_contour_index = 0;
			for (int i = 1; i < contours.size(); ++i) if (contours[i].size() > max_contour_size) max_contour_index = i;
			vector <Point> contour = contours[max_contour_index];
			drawContours(result, contours, max_contour_index, Scalar(0, 255, 0), 2);

			Moments m = moments(contour);
			if (m.m00 > 0) {
				Point mc = Point(static_cast<float>(m.m10 / (m.m00)), static_cast<float>(m.m01 / (m.m00)));

				Point l = contour[0];
				Point r = contour[0];
				Point t = contour[0];
				Point b = contour[0];
				for (int i = 1; i < contour.size(); ++i) {
					if (contour[i].x < l.x) l = contour[i];
					if (contour[i].x > r.x) r = contour[i];
					if (contour[i].y < t.y) t = contour[i];
					if (contour[i].y > b.y) b = contour[i];
				}

				int height = b.y - t.y;
				int width = r.x - l.x;
				int ev = height / 8;
				int eh = width / 8;

				err = mc.x - width / 2;

				auto nearby = [](Point _a, Point _b, int _ev, int _eh) {
					return _b.x - _eh <= _a.x && _a.x <= _b.x + _eh &&
					       _b.y - _ev <= _a.y && _a.y <= _b.y + _ev;
				};
				bool lt = nearby(l, t, ev, eh) || r.x < IMAGE_SIZE.width / 2;
				bool rt = nearby(r, t, ev, eh) || l.x > IMAGE_SIZE.width / 2;
				if (lt || rt) {
					partial = true;
					found = true;
					if (lt && rt) {
						found = false;
					} else if (lt) {
						left = true;
						right = false;
					} else if (rt) {
						left = false;
						right = true;
					}
				} else {
					partial = false;
					found = true;
					if (mc.x > IMAGE_SIZE.width / 2) {
						left = false;
						right = true;
					} else {
						left = true;
						right = false;
					}

					Point lb;
					for (int i = 0; i < contour.size(); ++i)
						if (contour[i].y >= lb.y || contour[i].y < b.y - ev) lb = contour[i];
						else if (contour[i].y > mc.y) break;
					Point rb;
					for (int i = contour.size() - 1; i >= 0; --i)
						if (contour[i].y >= rb.y || contour[i].y < b.y - ev) rb = contour[i];
						else if (contour[i].y > mc.y) break;

					if (nearby(lb, b, ev, eh) || nearby(rb, b, ev, eh)) {
						auto angle = [](Point _a, Point _b, Point _c) {
							Point ab = {_b.x - _a.x, _b.y - _a.y};
							Point cb = {_b.x - _c.x, _b.y - _c.y};
							float dot = (ab.x * cb.x + ab.y * cb.y);
							float cross = (ab.x * cb.y - ab.y * cb.x);
							float alpha = atan2(cross, dot);
							return (int) abs(floor(alpha * 180. / M_PI + 0.5));
						};
						if (angle(mc, lb, rb) > 50 || angle(mc, rb, lb) > 50) {
							diagonal = false;
							front = false;
							side = true;
						} else {
							diagonal = false;
							front = true;
							side = false;
						}
					} else {
						diagonal = true;
						front = false;
						side = false;
					}

					circle(result, mc, 4, Scalar(255, 255, 0), -1);
					circle(result, b, 4, Scalar(0, 255, 255), -1);
					circle(result, lb, 4, Scalar(255, 0, 0), -1);
					circle(result, rb, 4, Scalar(0, 0, 255), -1);
				}
			} else {
				found = false;
			}
		} else {
			found = false;
		}
		imshow(IMAGE_WINDOW, result);
		waitKey(3);
	}

	void laserScanCallback(sensor_msgs::LaserScan::ConstPtr msg) {
		if (msg->ranges[0] >= SAFE_DISTANCE && msg->ranges[359] >= SAFE_DISTANCE) safe = true;
		else safe = false;
	}

public:
	Dumpster() : imageTransport(nodeHandle) {
		cmdVelPublisher = nodeHandle.advertise<geometry_msgs::Twist>("/cmd_vel", 10);
		laserSubscriber = nodeHandle.subscribe("/scan", 1, &Dumpster::laserScanCallback, this);
		cameraSubscriber = imageTransport.subscribe("/camera/rgb/image_raw", 1, &Dumpster::cameraImageCallback, this);
		namedWindow(IMAGE_WINDOW);
	}

	~Dumpster() {
		destroyWindow(IMAGE_WINDOW);
	}

	void move(double range, double speed, bool rotate) {
		geometry_msgs::Twist cmd;
		if (range != 0) {
			if (!rotate) cmd.linear.x = speed;
			else cmd.angular.z = speed * M_PI / 180;
			double current = ros::Time::now().toSec();
			double end = current + abs(range / speed);
			while (current < end) {
				cmdVelPublisher.publish(cmd);
				current = ros::Time::now().toSec();
				ros::spinOnce();
			}
		}
		cmdVelPublisher.publish(geometry_msgs::Twist());
	}

	void forward() {
		geometry_msgs::Twist cmd;
		while (safe) {
			cmd.linear.x = 0.1;
			if (!partial) {
				if (left) cmd.angular.z = (float) err / IMAGE_SIZE.width;
				else if (right) cmd.angular.z = -(float) err / IMAGE_SIZE.width;
			} else {
				cmd.angular.z = 0;
			}
			cmdVelPublisher.publish(cmd);
			ros::spinOnce();
		}
		cmdVelPublisher.publish(geometry_msgs::Twist());
	}

	void search() {
		const int RATE = 5;
		ros::Rate rate(RATE);
		const int NOT_FOUND = RATE;
		const int NOT_SAFE = RATE;
		int not_found = 0;
		int not_safe = 0;
		while (ros::ok() && !done) {
			string result = "";
			if (found) {
				result.append("Found | ");
				if (safe) {
					result.append("Safe | ");
					if (partial) {
						result.append("Partial | ");
						if (left) {
							result.append("Left");
							move(15, SPEED_Z, true);
							if (front) move(SAFE_DISTANCE, -SPEED_X, false);
						} else if (right) {
							result.append("Right");
							move(15, -SPEED_Z, true);
						}
					} else {
						result.append("Full | ");
						if (diagonal) {
							result.append("Diagonal view");
							move(45, SPEED_Z, true);
							move(SAFE_DISTANCE, SPEED_X, false);
							move(90, -SPEED_Z, true);
						} else if (front) {
							result.append("Front view");
							forward();
							done = true;
						} else if (side) {
							result.append("Side view");
							move(90, SPEED_Z, true);
							move(SAFE_DISTANCE, SPEED_X, false);
							move(135, -SPEED_Z, true);
						}
					}
					not_safe = 0;
				} else {
					not_safe++;
					if (not_safe >= NOT_SAFE) {
						result.append("NOT SAFE");
						move(SAFE_DISTANCE, -SPEED_X, false);
					}
				}
				not_found = 0;
			} else {
				not_found++;
				if (not_found >= NOT_FOUND) {
					result.append("NOT FOUND");
					move(15, SPEED_Z, true);
				}
			}
			ROS_INFO("Dumpster: %s", result.c_str());
			ROS_INFO(
					"Dumpster: safe %i, found %i, diagonal %i, front %i, side %i, partial %i, left %i, right %i, done %i",
					safe, found, diagonal, front, side, partial, left, right, done
			);

			ros::spinOnce();
			rate.sleep();
		}
	}
};