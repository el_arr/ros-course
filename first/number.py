def tens(num):
    if num < 10:
        return 1
    else:
        return 10 * tens(num // 10)


def rec(num, ten):
    if num < 10:
        return num
    else:
        return num % 10 * ten + rec(num // 10, ten // 10)


number = int(input("Enter integer number: "))
print(rec(number, tens(number)))
