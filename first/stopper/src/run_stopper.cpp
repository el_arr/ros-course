#include "Stopper.h"

int main(int argc, char **argv) {
	ros::init(argc, argv, "stopper");
	Stopper stopper;

	ros::Rate rate(10);
	ROS_INFO("Start moving");

	while (ros::ok()) {
		stopper.moveForward();
		ros::spinOnce();
		rate.sleep();
	}

	return 0;
}