#!/usr/bin/env python
import math

import rospy
from geometry_msgs.msg import Twist
from sensor_msgs.msg import LaserScan


class Stopper:
	FORWARD_SPEED = 0.2
	ANGULAR_SPEED = 0.6
	ANGLE = 20.0
	MIN_SCAN_ANGLE = math.pi - (ANGLE / 180.0 * math.pi)
	MAX_SCAN_ANGLE = math.pi + (ANGLE / 180.0 * math.pi)
	MIN_DIST_FROM_OBSTACLE = 0.3
	rotating = False

	def __init__(self):
		self.pub = rospy.Publisher('/cmd_vel', Twist, queue_size=10)
		self.sub = rospy.Subscriber('scan', LaserScan, self.callback)

	def move(self):
		if not self.rotating:
			vel = Twist()
			vel.linear.x = (-1) * self.FORWARD_SPEED
			self.pub.publish(vel)

	def callback(self, msg):
		obstacle_in_front = False

		min_index = round((self.MIN_SCAN_ANGLE - msg.angle_min) / msg.angle_increment)
		max_index = round((self.MAX_SCAN_ANGLE - msg.angle_min) / msg.angle_increment)
		curr_index = int(min_index + 1)
		while curr_index <= max_index and not obstacle_in_front:
			if msg.ranges[curr_index] < self.MIN_DIST_FROM_OBSTACLE:
				obstacle_in_front = True
			curr_index = curr_index + 1

		if obstacle_in_front and not self.rotating:
			self.rotating = True
			min_index = round(msg.angle_min / msg.angle_increment)
			max_index = round(msg.angle_max / msg.angle_increment)
			curr_index = min_index + 1
			max_range = 0
			max_range_index = 0
			while curr_index <= max_index:
				curr_range = msg.ranges[int(curr_index)]
				if msg.range_min <= curr_range <= msg.range_max:
					if curr_range > max_range:
						max_range = curr_range
						max_range_index = curr_index
				curr_index = curr_index + 1
			t_angle = max_range_index * msg.angle_increment

			msg = Twist()
			if t_angle < math.pi:
				t_angle = math.pi - t_angle
				msg.angular.z = -self.ANGULAR_SPEED
			else:
				t_angle -= math.pi
				msg.angular.z = self.ANGULAR_SPEED
			self.pub.publish(msg)
			time = t_angle / self.ANGULAR_SPEED
			rospy.sleep(time)
		if not obstacle_in_front and self.rotating:
			t = Twist()
			self.pub.publish(t)
			self.rotating = False


if __name__ == '__main__':
	try:
		rospy.init_node('stopper', anonymous=False)
		rate = rospy.Rate(10)  # 10hz
		st = Stopper()
		while not rospy.is_shutdown():
			st.move()
			rate.sleep()
	except rospy.ROSInterruptException:
		pass