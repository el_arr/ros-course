#include <ros/ros.h>
#include <nav_msgs/GetMap.h>
#include "mapping/CleanMap.h"
#include <vector>
#include <fstream>

using namespace std;

int rows;
int cols;
vector<vector<bool> > grid;
nav_msgs::OccupancyGrid clean_map;

void readMap(const nav_msgs::OccupancyGrid &map) {
	ROS_INFO("Received a %d X %d map @ %.3f m/px\n", map.info.width, map.info.height, map.info.resolution);

	rows = map.info.height;
	cols = map.info.width;

	grid.resize(rows);
	for (int i = 0; i < rows; i++) { grid[i].resize(cols); }

	int currCell = 0;
	for (int i = 0; i < rows; i++) {
		for (int j = 0; j < cols; j++) {
			// Unoccupied (0), occupied (100) or unknown (-1) cells
			if (map.data[currCell] == 0)
				grid[i][j] = false;
			else
				grid[i][j] = true;
			currCell++;
		}
	}
}

bool cleanMap(ros::NodeHandle &nh, const nav_msgs::OccupancyGrid &map) {
	mapping::CleanMap::Request req;
	mapping::CleanMap::Response res;

	while (!ros::service::waitForService("clean_map", ros::Duration(3.0))) {
		ROS_INFO("Waiting for service clean_map to become available");
	}
	ROS_INFO("Cleaning the map");
	ros::ServiceClient mapClient = nh.serviceClient<mapping::CleanMap>("clean_map");

	req.map = map;
	if (mapClient.call(req, res)) {
		clean_map = res.map;
		readMap(clean_map);
		return true;
	} else {
		ROS_ERROR("Failed to call clean map service");
		return false;
	}
}

bool requestMap(ros::NodeHandle &nh) {
	nav_msgs::GetMap::Request req;
	nav_msgs::GetMap::Response res;

	while (!ros::service::waitForService("static_map", ros::Duration(3.0))) {
		ROS_INFO("Waiting for service static_map to become available");
	}

	ROS_INFO("Requesting the map...");
	ros::ServiceClient mapClient = nh.serviceClient<nav_msgs::GetMap>("static_map");

	if (mapClient.call(req, res)) {
		return cleanMap(nh, res.map);
	} else {
		ROS_ERROR("Failed to call map service");
		return false;
	}
}

void printGridToFile(const nav_msgs::OccupancyGrid &map) {
	ROS_INFO("Print %d X %d map to file grid.txt", map.info.width, map.info.height);
	std::ofstream gridFile;
	gridFile.open("grid.txt");

	int currCell = 0;
	for (int i = 0; i < rows; i++) {
		for (int j = 0; j < cols; j++) {
			if (map.data[currCell] == 0)
				gridFile << "0";
			if (map.data[currCell] == 100)
				gridFile << "1";
			if (map.data[currCell] == -1)
				gridFile << "-";
			currCell++;
		}
		gridFile << endl;
	}
	gridFile.close();
}

int main(int argc, char **argv) {
	ros::init(argc, argv, "map_loader");
	ros::NodeHandle nh;
	ros::Publisher mapPub = nh.advertise<nav_msgs::OccupancyGrid>("/new_map", 10);

	if (!requestMap(nh))
		exit(-1);

	printGridToFile(clean_map);

	ros::Rate rate(10);
	while (ros::ok()) {
		mapPub.publish(clean_map);
		ros::spinOnce();
		rate.sleep();
	}

	return 0;
}