#!/usr/bin/env python3  
import rospy
import tf
from nav_msgs.srv import GetMap

res = 0.0
w = 0
h = 0
ox = 0
oy = 0


def cell_to_pos(i, j):
	global ox
	global oy
	global res
	x = i * res + ox
	y = j * res + oy
	return x, y


def pos_to_cell(x, y):
	global ox
	global oy
	global res
	x_delta = x - ox
	i = x_delta // res
	if x_delta < 0:
		i -= 1
	y_delta = y - oy
	j = y_delta // res
	if y_delta < 0:
		j -= 1
	return i, j


def request_map():
	global res
	global h
	global w
	global ox
	global oy
	rospy.wait_for_service('static_map')
	try:
		static_map = rospy.ServiceProxy("static_map", GetMap)
		st_map = static_map().map
		res = st_map.info.resolution
		print("resolution ", res)
		h = st_map.info.height
		w = st_map.info.width
		ox = st_map.info.origin.position.x
		oy = st_map.info.origin.position.y

	except rospy.ServiceException as e:
		print("Service call failed: %s", e)
	rospy.loginfo("Write to grid finished")


if __name__ == '__main__':
	rospy.init_node('robot_location')
	request_map()
	listener = tf.TransformListener()
	rate = rospy.Rate(2.0)
	while not rospy.is_shutdown():
		try:
			(trans, rot) = listener.lookupTransform('/loaded_map', '/base_footprint', rospy.Time(0))
			rospy.logwarn("Position: (%d, %d)" % (trans[0], trans[1]))
			rospy.logwarn("Cell: (%d, %d)" % pos_to_cell(trans[0], trans[1]))
			rate.sleep()
		except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
			continue