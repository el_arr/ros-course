#include <ros/ros.h>
#include <tf/transform_listener.h>
#include <nav_msgs/GetMap.h>
#include <utility>

using namespace std;
double res = 0;
int w = 0;
int h = 0;
double ox = 0;
double oy = 0;

pair<double, double> cellToPos(int i, int j) {
	double x = i * res + ox;
	double y = j * res + oy;
	return make_pair(x, y);
}

pair<int, int> posToCell(double x, double y) {
	int x_delta = x - ox;
	int i = x_delta / res;
	if (x_delta < 0) i -= 1;
	int y_delta = y - oy;
	int j = y_delta / res;
	if (y_delta < 0) j -= 1;
	return make_pair(i, j);
}

bool requestMap(ros::NodeHandle &nh) {
	nav_msgs::GetMap::Request req;
	nav_msgs::GetMap::Response resp;

	while (!ros::service::waitForService("static_map", ros::Duration(3.0))) {
		ROS_INFO("Waiting for service static_map to become available");
	}

	ROS_INFO("Requesting the map...");
	ros::ServiceClient mapClient = nh.serviceClient<nav_msgs::GetMap>("static_map");

	if (mapClient.call(req, resp)) {
		res = resp.map.info.resolution;
		w = resp.map.info.width;
		h = resp.map.info.height;
		ox = resp.map.info.origin.position.x;
		oy = resp.map.info.origin.position.y;
		return true;
	} else {
		ROS_ERROR("Failed to call map service");
		return false;
	}
}


int main(int argc, char **argv) {
	ros::init(argc, argv, "robot_location");
	ros::NodeHandle node;

	if (!requestMap(node))
		exit(-1);

	tf::TransformListener listener;
	ros::Rate rate(2.0);
	listener.waitForTransform("/loaded_map", "/base_footprint", ros::Time(0), ros::Duration(10.0));
	while (ros::ok()) {
		tf::StampedTransform transform;
		try {
			listener.lookupTransform("/loaded_map", "/base_footprint", ros::Time(0), transform);
			double x = transform.getOrigin().x();
			double y = transform.getOrigin().y();
			auto p = posToCell(x, y);
			ROS_INFO("Position: (%f, %f)", x, y);
			ROS_INFO("Cell: (%d, %d)", p.first, p.second);
		} catch (tf::TransformException &ex) {
			ROS_ERROR("%s", ex.what());
		}
		rate.sleep();
	}

	return 0;
}