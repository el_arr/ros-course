#!/usr/bin/env python

import math
import rospy
import sys
from geometry_msgs.msg import Twist
from turtlesim.msg import Pose


class Turtle:
	LINEAR_SPEED = 1
	initial_pos = [0, 0, 0]
	current_pos = [0, 0, 0]

	def __init__(self):
		rospy.init_node('turtle_node', anonymous=False)
		rospy.loginfo("To stop TurtleBot press \"CTRL + C\"")
		rospy.on_shutdown(self.shutdown)

		robot_name = sys.argv[1]
		robot_distance = int(sys.argv[2])
		robot_rotation = (int(sys.argv[3]) % 360) * math.pi / 180
		if robot_rotation > math.pi:
			robot_rotation = robot_rotation - (2 * math.pi)

		self.vel_pub = rospy.Publisher(
			name=robot_name + '/cmd_vel',
			data_class=Twist,
			queue_size=10
		)
		self.pos_sub = rospy.Subscriber(
			name=robot_name + '/pose',
			callback=self.pose_callback,
			data_class=Pose,
			queue_size=10
		)
		r = rospy.Rate(10)

		while not rospy.is_shutdown() and abs(self.current_pos[0] - self.initial_pos[0]) < robot_distance:
			vel = Twist()
			vel.linear.x = self.LINEAR_SPEED
			self.vel_pub.publish(vel)
			r.sleep()

		while not rospy.is_shutdown() and \
				round(abs(self.current_pos[2] - self.initial_pos[2]), 1) < round(abs(robot_rotation), 1):
			rospy.loginfo(
				"1: {:.2f}, 2: {:.2f}, 3: {:.2f}".format(self.current_pos[2], self.initial_pos[2], robot_rotation))
			vel = Twist()
			vel.angular.z = robot_rotation
			self.vel_pub.publish(vel)
			r.sleep()

	def pose_callback(self, msg):
		rospy.loginfo("x: {:.2f}, y: {:.2f}, theta: {:.2f}".format(msg.x, msg.y, msg.theta))
		if self.initial_pos[0] == 0 and self.initial_pos[1] == 0:
			self.initial_pos[0] = msg.x
			self.initial_pos[1] = msg.y
			self.initial_pos[2] = msg.theta
		self.current_pos[0] = msg.x
		self.current_pos[1] = msg.y
		self.current_pos[2] = msg.theta

	def shutdown(self):
		rospy.loginfo("Stopping TurtleBot")
		self.vel_pub.publish(Twist())
		rospy.sleep(1)


if __name__ == '__main__':
	try:
		Turtle()
	except rospy.ROSInterruptException:
		pass